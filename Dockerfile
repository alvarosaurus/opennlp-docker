FROM aot29/spring:latest

ADD resources/apache-opennlp-1.9.1-bin.tar.gz /opt

ENV PATH /opt/apache-opennlp-1.9.1/bin:$PATH

