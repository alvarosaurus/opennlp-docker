A Docker container for natural language processing using Apache OpenNLP, Spring Maven and OpenJDK.
A ready Docker image can be downloaded from: https://cloud.docker.com/repository/docker/aot29/opennlp

# Building and starting
Build (the first time around) and run the image: `docker-compose up -d`
This will create a directory `workspace` on the host, and mount it as `/workspace`in the container. Files in this directory will persist if you restart the container.

# Executing the example project
Open a console: `docker exec -ti opennlp bash`

see:
* https://opennlp.apache.org/docs/1.9.1/manual/opennlp.html
* https://www.tutorialspoint.com/opennlp/opennlp_environment.htm
